let saveBtn;
let textInput;
let fileInput;
let eraserBtn;
let destroyBtn;
let modeBtn;
let colorOptions;
let color;
let lineWidth;
let canvas;
let ctx;
let CANVAS_WIDTH;
let CANVAS_HEIGHT;
let isPainting;
let isFilling;

let colorValue;

let file;
let url1;
let image;

let text;

let url2;
let a;

function onMove(event) {
    if (isPainting) {
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        return;
    }
    ctx.moveTo(event.offsetX, event.offsetY);
}
function startPainting() {
    isPainting = true;
}
function cancelPainting() {
    isPainting = false;
    ctx.beginPath();
}
function onLineWidthChange(event) {
    ctx.lineWidth = event.target.value;
}
function onColorChange(event) {
    ctx.strokeStyle = event.target.value;
    ctx.fillStyle = event.target.value;
}
function onColorClick(event) {
    colorValue = event.target.dataset.color;
    ctx.strokeStyle = colorValue;
    ctx.fillStyle = colorValue;
    color.value = colorValue;
}
function onModeClick() {
    if (isFilling) {
        isFilling = false;
        modeBtn.innerText = "Fill";
    } else {
        isFilling = true;
        modeBtn.innerText = "Draw";
    }
}
function onCanvasClick() {
    if (isFilling) {
        ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    }
}
function onDestroyClick() {
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
}
function onEraserClick() {
    ctx.strokeStyle = "white";
    isFilling = false;
    modeBtn.innerText = "Fill";
}
function onFileChange(event) {
    file = event.target.files[0];
    url1 = URL.createObjectURL(file);
    image = new Image();
    image.src = url1;
    image.onload = function () {
        ctx.drawImage(image, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        fileInput.value = null;
    };
}
function onDoubleClick(event) {
    text = textInput.value;
    if (text !== "") {
        ctx.save();
        ctx.lineWidth = 1;
        ctx.font = "68px sans-serif";
        ctx.fillText(text, event.offsetX, event.offsetY);
        ctx.restore();
    }
}

function onSaveClick() {
    url2 = canvas.toDataURL();
    a = document.createElement("a");
    a.href = url2;
    a.download = "myDrawing.png";
    a.click();
}

export function runCanvas() {
    saveBtn = document.getElementById("save");
    textInput = document.getElementById("text");
    fileInput = document.getElementById("file");
    eraserBtn = document.getElementById("eraser-btn");
    destroyBtn = document.getElementById("destroy-btn");
    modeBtn = document.getElementById("mode-btn");
    colorOptions = Array.from(document.getElementsByClassName("color-option"));
    color = document.getElementById("color");
    lineWidth = document.getElementById("line-width");
    canvas = document.querySelector("canvas");
    ctx = canvas.getContext("2d");
    CANVAS_WIDTH = 800;
    CANVAS_HEIGHT = 800;
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;
    ctx.lineWidth = lineWidth.value;
    ctx.lineCap = "round";
    isPainting = false;
    isFilling = false;

    canvas.addEventListener("dblclick", onDoubleClick);
    canvas.addEventListener("mousemove", onMove);
    canvas.addEventListener("mousedown", startPainting);
    canvas.addEventListener("mouseup", cancelPainting);
    canvas.addEventListener("mouseleave", cancelPainting);
    canvas.addEventListener("click", onCanvasClick);
    lineWidth.addEventListener("change", onLineWidthChange);
    color.addEventListener("change", onColorChange);
    colorOptions.forEach((color) =>
        color.addEventListener("click", onColorClick)
    );
    modeBtn.addEventListener("click", onModeClick);
    destroyBtn.addEventListener("click", onDestroyClick);
    eraserBtn.addEventListener("click", onEraserClick);
    fileInput.addEventListener("change", onFileChange);
    saveBtn.addEventListener("click", onSaveClick);
}
