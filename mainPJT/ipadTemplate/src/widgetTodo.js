$(document).ready(function () {

	//데이터 업데이트 및 localStorage에 저장
	function updateText() {
		$('#wt-count').text(wtTotal);
		$('#wt-count_done').text(wt_check_count);
		$('#wt-remaining_done').text(wtTotal - wt_check_count);

		saveTodosToLocalStorage(); // Save to localStorage
	}

	//app-header__date update
	function showDate() {
		let date = new Date();
		let dayOfMonth = date.getDate();
		let dayOfWeek = date.getDay();
		let Month = date.getMonth();

		let $today = $('#wt-today');
		let $daymonth = $('#wt-daymonth');
		let $month = $('#wt-month');

		const dayArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		const monthArray = ['January', 'February', 'March', 'April,', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

		const suffixes = ['th', 'st', 'nd', 'rd'];
		let suffixeIndex = dayOfMonth % 10 == 0 || dayOfMonth % 10 >= 4 ? 0 : dayOfMonth % 10;
		let suffix = suffixes[suffixeIndex];

		$today.text(dayArray[dayOfWeek] + ',');
		$daymonth.text(' ' + dayOfMonth + suffix);
		$month.text(monthArray[Month]);
	}

	//Load todos from localStorage
	function loadTodosFromLocalStorage() {
		let storedTodos = localStorage.getItem('todos');
		if (storedTodos) {
			wt_todos = JSON.parse(storedTodos); //JSON 문자열을 객체나 배열로 변환하는 메소드
			wtTotal = wt_todos.length;

			wt_todos.forEach(todo => {
				let isChecked = 'far fa-circle mark-alt';
				let contentClass = '';
				let contentEditable = true;
				let todoContentDone = '';
				if (todo.checked) {
					isChecked = 'fa fa-check-circle mark';
					contentClass = 'wt-todo__content--done';
					contentEditable = false;
					todoContentDone = 'wt-todo__content--done';
					wt_check_count++;
				}

				let load_li = `<li class="wt-todo"><button href="" class="wt-todo__check_button" onmousedown="return false"><i class="${isChecked} wt-check_icon" aria-hidden="true"></i></button><div class="wt-todo__content"><p contenteditable="${contentEditable}" class="${todoContentDone}">${todo.content}</p></div><button class="wt-todo__delete_button" onmousedown="return false"><i class="fa fa-times-circle wt-delete_icon" aria-hidden="true"></i></button></li>`;

				$('.wt-app-main__container').append(load_li);
			});
		}
		updateText();
	}

	//Save todos to localStorage
	function saveTodosToLocalStorage() {
		localStorage.setItem('todos', JSON.stringify(wt_todos));
	}

	let wtTotal = 0;
	let wt_check_count = 0;
	let wt_todos = [];

	loadTodosFromLocalStorage(); //localStorage의 데이터 불러오기

	//시작시 애니메이션
	$('.wt-add_todo').addClass('wt-show');
	$('.wt-app-main__container .wt-todo').each(function (j) {
		let $this = $(this);
		setTimeout(function () {
			$this.addClass('wt-down_in').removeClass('wt-todohidden');
			setTimeout(function () {
				$this.removeAttr('class').addClass('wt-todo');
			}, 550);
		}, 60 * j);
	});

	//Click button을 선택하여 todo 추가
	$('.wt-add_todo #wt-add-new').click(function (e) {
		e.preventDefault();

		let newTodoContent = 'New Task';
		wt_todos.push({ content: newTodoContent, checked: false });
		let created_li = `<li class="wt-todo"><button href="" class="wt-todo__check_button" onmousedown="return false"><i class="far fa-circle wt-check_icon" aria-hidden="true"></i></button><div class="wt-todo__content"><p contenteditable="true">${newTodoContent}</p></div><button class="wt-todo__delete_button" onmousedown="return false"><i class="fa fa-times-circle wt-delete_icon" aria-hidden="true"></i></button></li>`;
		
		$('.wt-app-main__container').append(created_li).find('li:last-child').addClass('wt-down');
		
		setTimeout(function() {
			$('.wt-app-main__container').scrollTop($('.wt-app-main__container')[0].scrollHeight);
		}, 170);
		
		wtTotal += 1;
		updateText();
	});

	// Click on button function list - todo check 버튼 클릭 시 이벤트
	$('.widget-todo-app .wt-app-main__container').on('click', '.wt-todo .wt-todo__check_button', function (e) {
		e.preventDefault();

		let button = $(this).find('i');
		let checked = 'fa fa-check-circle mark';
		let unchecked = 'far fa-circle';

		// Save the current index of button after the click event in the "left" div.
		let index_click = $('.wt-todo .wt-todo__check_button').index(this);
		// Use the current index of button to target the correct "li p" in the "right" div.
		let linethrough_text = $('.wt-todo .wt-todo__content').eq(index_click).find('p');

		if (button.hasClass(unchecked)) {
			linethrough_text.addClass('wt-todo__content--done').attr('contentEditable', false);
			button
				.removeClass(unchecked + ' mark-alt')
				.addClass('wt-pop_in')
				.addClass(checked);
			wt_todos[index_click].checked = true;
			wt_check_count++;
		} else {
			linethrough_text.removeClass('wt-todo__content--done').attr('contentEditable', true);
			button
				.removeClass(checked)
				.removeClass('wt-pop_in')
				.addClass(unchecked + ' mark-alt');
			wt_todos[index_click].checked = false;
			wt_check_count--;
		}

		updateText();
	});

	//todo content 변경 시 이벤트
	$('.widget-todo-app .wt-app-main__container').on('input', '.wt-todo .wt-todo__content p', function (e) {
		let index_click = $('.wt-todo .wt-todo__content p').index(this);
		wt_todos[index_click].content = $(this).text();

		updateText();
	});

	// Click on button function and delete 'li' - delete버튼 클릭 시 이벤트
	$('.widget-todo-app .wt-app-main__container').on('click', '.wt-todo .wt-todo__delete_button', function (e) {
		e.preventDefault();
		let index_click = $('.wt-todo .wt-todo__delete_button').index(this);
		wt_todos.splice(index_click, 1);

		let current = $('.wt-todo').eq(index_click);
		let button = $('.wt-todo__check_button').find('i');

		$(this).prop('disabled', true);
		wtTotal--;

		if (button.eq(index_click).hasClass('mark')) {
			wt_check_count--;
		}

		current.addClass('wt-up');
		setTimeout(function () {
			current.remove();
		}, 560);

		updateText();
	});

	showDate();
});
