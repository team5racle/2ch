let gender;

let passwordRegex;
let passwordField;
let passwordConfirmField;
let password;

let defaultOption;

// gender-toggle 남녀 토글
const handlerToggleGender = (gender) => {
    if (gender === "male") {
        document.querySelector(".male").classList.add("active");
        document.querySelector(".female").classList.remove("active");
    } else {
        document.querySelector(".male").classList.remove("active");
        document.querySelector(".female").classList.add("active");
    }
};

export function signup() {
    // gender-toggle 남녀 토글
    document.querySelectorAll(".gender-toggle span").forEach((span) => {
        span.addEventListener("click", function () {
            gender = this.classList.contains("male") ? "male" : "female";
            handlerToggleGender(gender);
        });
    });

    // 비밀번호 유효성 검사
    passwordRegex =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,16}$/;
    passwordField = document.querySelector("input[name='password']");

    document.querySelector("form").addEventListener("submit", (e) => {
        if (!passwordRegex.test(passwordField.value)) {
            alert(
                "비밀번호: 10~16자의 영문 대/소문자, 숫자, 특수문자를 최소 1개씩 사용해 주세요."
            );
            passwordField.focus();
            e.preventDefault();
        }
    });

    // 비밀번호 확인
    passwordConfirmField = document.querySelector(
        "input[name='password_confirm']"
    );

    document.querySelector("form").addEventListener("submit", (e) => {
        password = document.querySelector("input[name='password']");
        if (password.value !== passwordConfirmField.value) {
            alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
            e.preventDefault();
        }
    });

    // location-dropdown "Location" 숨기기
    document.getElementById("location").addEventListener("change", () => {
        defaultOption = document.querySelector("option[value='']");
        if (defaultOption) defaultOption.style.display = "none";
    });
}
