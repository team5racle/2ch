let $audio
//music player 관련 노드
let $skipBackBtn
let $playBtn
let $skipForwardBtn

//플레이어독 왼쪽 음악정보 부분 노드
let $songCoverImage
let $songArtist
let $songTitle
//음악시간 관련 노드
let $currentMusicTimeSpan
let $currentMusicTotalTimeSpan
let $progressBarDiv
let $playerProgressBarDoneDiv

//볼륨관련 노드
let $volumeBtnSpan
let $volumeProgressBarDiv
let $volumeProgressBarDoneDiv

//한곡반복 , 셔플
let $playerRepeatSpan
let $playerShuffleSpan
//음악검색
let $musicSearchInput

//플레이리스트 , 전체음악
let $allmusicA
let $playListsNav

//메인화면 앨범커버
let $artworks

//볼륨 업다운
let $volumeUp
let $volumeDown

let musicJsonArray; // 모든 음악에 대한 json 배열
let totalMusicCount; // 모든 음악 개수
let musicIndex ;  // currentPlayListIndices 배열에서 사용하기위한 인덱스
let currentPlayListIndices; // 현재 화면에 보이는 플레이리스트의 인덱스정보 (인덱스 == 음악 json 배열의 인덱스)
let currentPlayListMusicCount; // 현재 화면에 보이는 플레이리스트의 음악갯수
let articleIndex ; // 음악 앨범 게시글에 대한 인덱스
let isPlaying; // 재생중인지 체크 변수
let isShuffle ; //셔플이 체크되어있는지 여부

let currentMusicTitle;
let currentMusicArtist;
let currentImgPath;
let currentAudioPath;

//음악정보가 저장되어있는 music.json 파일에서 데이터를 가져온다.
function loadMusics() {
    return fetch("./data/music.json")
        .then(response => response.json())
        .then(json => {
            musicJsonArray = json.musics; //가져온 데이터를 musicJsonArray 에 담는다. musicJsonArray 는 모든 음악 정보를 가지고있다.
            totalMusicCount=musicJsonArray.length; // totalMusicCount 에는 전체 음악 개수를 담는다.
            return json.musics; //음악 json 배열을 리턴한다.
        });
}

// 음악 json 데이터를 이용하여 음악 아티클을 생성한다.
function createArticle(music) {
    let article = document.createElement("article");
    article.classList.add("artwork")
    article.style.setProperty("--img",`url(${music.imgPath})`) ;
    //article 에는 그 article 에 해당하는 음악의 index 를 가지고있는다. ( index == 모든 음악 json 배열에서 사용하는 인덱스)
    article.setAttribute("data-article-index", articleIndex.toString());
    currentPlayListIndices.push(articleIndex);
    articleIndex++;
    article.addEventListener('click', clickArticle);
    let bgDiv = document.createElement("div");
    bgDiv.classList.add("artwork__bg");
    let detailDiv = document.createElement("div");
    detailDiv.classList.add("artwork__details");

    let innerDiv = document.createElement("div");
    innerDiv.classList.add("artwork__details-inner");

    let artistDiv = document.createElement("div");
    artistDiv.classList.add("artwork-artist");
    artistDiv.style.color = "black";
    artistDiv.innerText = music.artist;

    let songDiv = document.createElement("div");
    songDiv.classList.add("artwork-song");
    songDiv.style.color = "black";
    songDiv.innerText=music.songTitle;

    innerDiv.append(artistDiv,songDiv);
    detailDiv.append(innerDiv);
    article.append(bgDiv, detailDiv);
    return article;
}

//오디오 설정을 초기화 하는 부분
function initAudio() {
    $audio.volume=0.5 ; // 볼륨 0.5 초기화
    loadMusic();  // 현재 선택된 음악정보를 변수에 담는다.
    // 최초로 선택되어있는 음악은 0번 인덱스의 음악이다. 인덱스는 음악데이터의 json 배열 인덱스를 기준으로 말한다.
    $audio.setAttribute("src",currentAudioPath);
    isPlaying=false; // 현재 음악이 재생되고있는지를 체크하는 변수도 초기화해준다.
    setMusicContent(); // 음악플레이어 왼쪽에 현재 선택된 음악의 이미지,노래제목,가수를 설정하는 함수
}

//음악 재생 정지 토글 -> 재생버튼에 추가하는 이벤트리스너함수이다.
function playMusic() {
    if(isPlaying){ // 음악이 재생되고 있다면
        $audio.pause(); // 음악을 정지하고
        $playBtn.querySelector("svg").setAttribute("data-feather", "play"); // 아이콘을 재생아이콘으로 변환한다.
        isPlaying=false; // 현재 재생중인지 체크하는 변수를 false 로 설정한다.
    }else{ // 음악이 재생되고 있지 않다면
        $audio.play(); // 음악을 재생하고
        $playBtn.querySelector("svg").setAttribute("data-feather", "pause"); // 아이콘을 정지아이콘으로 변환한다.
        isPlaying=true; // 현재 재생중인지 체크하는 변수를 true 로 설정한다.
    }
    feather.replace(); // feather 라는 icon 라이브러리의 함수를 통해 "data-feather" 속성을 가진 노드를 svg 태그의 아이콘으로 변환시킨다.
}

// 다음곡버튼,이전곡버튼, 현재 노래가 끝나서 다음 노래로 넘어 갈때 등에 사용되는 재생함수이다.
function justPlay() {
    if (totalMusicCount === 0) {
        return;
    }
    loadMusic(); // 현재 선택된 노래의 제목,가수,음악파일경로,앨범커버경로를 변수에 저장해둔다.
    setMusicContent(); // 음악플레이어 왼쪽에 현재 선택된 음악의 이미지,노래제목,가수를 설정하는 함수
    // $audio.setAttribute("src",`${musicJsonArray[currentPlayListIndices[Number(musicIndex)]].audioPath}`);
    $audio.setAttribute("src",currentAudioPath); // 오디오 노드는 현재 선택된 곡으로 설정한다.
    $audio.play(); // 음악 재생
    $playBtn.querySelector("svg").setAttribute("data-feather", "pause"); // 정지아이콘으로 변경하기 위해 속성변경
    isPlaying=true; // 재생중으로 변경
    feather.replace(); //아이콘 변환
}

//다음곡 버튼 클릭이벤트 , 노래재생이 끝나서 다음으로 넘어갈때 사용되는 함수
function playNextMusic() {
    //한복 반복재생일때는 인덱스에 대한 변경을 진행하지않는다. -> 같은곡을 반복하게끔한다.
    if ($audio.loop) {
    }
    else if (isShuffle) { //셔플기능이 켜져있을때
        if(currentPlayListMusicCount !== 1) // 현재 플레이리스트의 음악개수가 1개가 아니여야 셔플이 동작한다.1개라면 반복재생한다.
        {
            while (true) {
                // ( 0  ~  현재 플레이리스트의 음악개수 -1 ) 범위를 가지는 숫자를 뽑는다. 이 숫자는 현재 플레이리스트 인덱스 배열의 인덱스로 사용된다.
                let randomIndex = Math.floor(Math.random()*(currentPlayListMusicCount));
                if (randomIndex !== musicIndex) { // 현재 선택된 곡이랑 같지않을때까지 반복한다.
                    musicIndex = randomIndex;
                    break;
                }
            }
        }
    }else{  //일반재생일때
        // 다음곡으로 넘겨야하므로 인덱스에 +1를 진행해준다. % 를 해주는 이유는 값이 플레이리스트 음악 전체 개수를 넘었을때 0번째부터 다시 시작하게끔하기 위함.
        musicIndex = (Number(musicIndex) + 1) % currentPlayListMusicCount;
    }
    justPlay(); // 음악 재생
}

//이전곡 버튼의 클릭 이벤트 리스너 함수
function playBeforeMusic() {
    // 첫번째 노래면 끝노래가 선택되게끔한다. 첫번째 노래가 아니라면 음악인덱스 -1 를 해주어서 이전곡을 선택한다.
    musicIndex = musicIndex === 0 ? Number(currentPlayListMusicCount) - 1 : Number(musicIndex) - 1;
    // $audio.setAttribute("src",`${musicJsonArray[musicIndex].audioPath}`);
    // loadMusic();
    console.log(`현재음악 : ${currentMusicTitle}`);
    justPlay(); // 음악 재생
    // setMusicContent();
}

//현재 선택된 노래의 제목,가수,음악파일경로,앨범커버경로를 변수에 저장해둔다.
function loadMusic() {
    console.log("musicIndex : "+musicIndex);
    console.log("musicJsonArray : "+musicJsonArray);

    /*
    musicJsonArray : 모든 음악에 대한 정보가 들어있는 json 배열  ex) [ {음악제목,가수이름,음악경로,이미지경로} , {} , {} ...]
    currentPlayListIndices : 현재 선택된 플레이리스트에 대한 인덱스들이 저장되어있는 배열  ex) [3,4,5]
    인덱스란 : musicJsonArray 라는 json 배열의  인덱스를 의미한다.
    musicIndex : currentPlayListIndices 배열 에서 값을 뽑기 위해 사용되는 변수이다.
        예를들어 다음곡버튼을 누르면 musicIndex 가 +1이 되고 이전곡버튼을 누르면 musicindex 가 -1 이 된다.
    */

    currentMusicTitle = musicJsonArray[currentPlayListIndices[musicIndex]].songTitle;
    currentMusicArtist= musicJsonArray[currentPlayListIndices[musicIndex]].artist;
    currentAudioPath = musicJsonArray[currentPlayListIndices[musicIndex]].audioPath;
    currentImgPath = musicJsonArray[currentPlayListIndices[musicIndex]].imgPath;
    console.log(currentMusicTitle);
    console.log(currentMusicArtist);
    console.log(currentAudioPath);
    console.log(currentImgPath);
}

// 현재 선택된 음악의 이미지,노래제목,가수를 음악플레이어 왼쪽에 설정하는 함수
function setMusicContent() {
    //loadMusic()를 통해 현재 선택된 음악에 대한 정보가 변수에 저장되고 , 그 변수를 통해 음악플레이어 왼쪽의 음악정보를 구성해준다.
    $songTitle.innerText = currentMusicTitle;
    $songArtist.innerText = currentMusicArtist
    $songCoverImage.setAttribute("src", currentImgPath.slice(1));
}

//음악 진행 바를 실시간으로 바꾸기 위한 이벤트리스터함수
function updateMusicProgressbar(event) {
    let currentTime = event.target.currentTime; //  현재 재생중인 음악의 진행된 시간을 받는다.
    let duration = event.target.duration; // 재생중인 음악의 전체시간을 받는다.
    let progressRate = (currentTime / duration) * 100; // 현재시간과 전체시간을 이용하여 음악이 진행된 비율을 구한다.
    $playerProgressBarDoneDiv.style.width = `${progressRate}%`; // 진행바의 완료부분을 표시하는 DIV 의 width 를 진행된 비율만큼으로 설정.
    // 아래코드는 현재 진행된 시간을 표시하기 위한 처리
    let curMin = Math.floor(currentTime/60);
    let curSec = Math.floor(currentTime % 60);
    if(curSec<10) curSec = `0${curSec}`;
    let curTimeString = `${curMin}:${curSec}`;
    $currentMusicTimeSpan.innerText = curTimeString;
}

//새로운 음악이 선택되면 총 재생시간을 변경시켜주기 위한 함수
function updateDuration(event) {
    let duration = event.target.duration;
    let durMin = Math.floor(duration/60);
    let durSec = Math.floor(duration % 60);
    if(durSec<10) durSec = `0${durSec}`;
    $currentMusicTotalTimeSpan.innerText = `${durMin}:${durSec}`;
}

// 음악 재생바를 클릭했을때 그 시간대로 이동하기 위한 함수
function musicProgressBarClick(event) {
    let musicProgressbarMaxWidth = $progressBarDiv.clientWidth; //음악재생바의 전체 크기를 구하고
    let clickX = event.offsetX; // 음악재생바 기준 클릭된 X 좌표를 구한다.
    let duration = $audio.duration; // 현재 음악 전체시간을 가져온다.
    console.log(musicProgressbarMaxWidth);
    console.log(clickX);
    console.log(duration);
    $audio.currentTime=(clickX/musicProgressbarMaxWidth)*duration; // 클릭된 재생바의 비율을 이용하여 재생중인 시간을 설정한다.
}

// 볼륨바를 클릭했을때 볼륨을 변경하기 위한 함수
function volumeBarClick(event) {
    let volumeBarWidth = $volumeProgressBarDiv.clientWidth; //볼륨바의 전체 크기를 구한다.
    let clickX = event.offsetX; //볼륨바 기준 클릭된 x좌표를 구한다.
    console.log(volumeBarWidth);
    console.log(clickX);
    console.log((clickX/volumeBarWidth))
    $audio.volume=(clickX/volumeBarWidth); // 볼륨은 0 ~ 1 이다.  비율을 통해 볼륨을 재설정한다.
    $volumeProgressBarDoneDiv.style.width= `${(clickX/volumeBarWidth)*100}%`; // 볼륨바의 넓이또한 비율로 설정해준다.
}


// 음악을 클릭했을경우 해당 노래를 재생하기 위한 함수 ( 음악 == 앨범커버 == Article)
function clickArticle(event) {
    console.log("event.currentTarget : "+event.currentTarget);
    let clickedArticleIndex = Number(event.currentTarget.getAttribute('data-article-index'));
    console.log("clickedArticleIndex : "+clickedArticleIndex);
    console.log("currentPlayListMusicCount : " + currentPlayListMusicCount);
    if (musicIndex !== clickedArticleIndex) { // 이미 현재 선택된 노래는 재생하지않는다.
        // if (clickedArticleIndex === 0) {
        //     musicIndex = 0;
        // } else {
        //     musicIndex = Number(clickedArticleIndex%currentPlayListMusicCount);
        // }
        musicIndex = Number(clickedArticleIndex%currentPlayListMusicCount);
        console.log("musicIndex : " + musicIndex);
        justPlay();
    }

}

//음소거 기능 토글 함수
function mute() {
    if($audio.muted){ // 이미 음소거라면
        $audio.muted=false; // 음소거 해제
        $volumeBtnSpan.querySelector("svg").setAttribute("data-feather", "volume-2"); // 일반 볼륨 아이콘으로 설정
    }else{
        $audio.muted=true;
        $volumeBtnSpan.querySelector("svg").setAttribute("data-feather", "volume-x"); // 음소거 아이콘으로 설정
    }
    feather.replace(); // 아이콘 변경
}

//한곡 반복기능을 위한 토글 함수
function repeat() {
    if (isShuffle) { // 셔플기능이 켜져있다면 셔플기능을 종료해주고 셔플 아이콘의 색을 원래대로 변경시킨다.
        isShuffle = false;
        $playerShuffleSpan.querySelector("svg").style.stroke = "";
        $playerShuffleSpan.querySelector("svg").style.fill = "";
    }
    if ($audio.loop) { // 한곡 반복이 이미 진행중이라면 반복기능을 해제하고, 아이콘 또한 원래대로 바꾼다.
        $audio.loop = false;
        $playerRepeatSpan.querySelector("svg").style.stroke = "";
        $playerRepeatSpan.querySelector("svg").style.fill = "";
    }else{ // 한곡 반복이 진행중이 아니라면 반복기능을 설정해주고, 아이콘 또한 색을 변경한다.
        $audio.loop = true;
        $playerRepeatSpan.querySelector("svg").style.stroke = "skyblue";
        $playerRepeatSpan.querySelector("svg").style.fill = "skyblue";

    }
}

//셔플기능을 위한 토글 함수
function shuffle() {
    if ($audio.loop) { // 한곡 반복이 이미 진행중이라면 반복기능을 해제하고, 아이콘 또한 원래대로 바꾼다.
        $audio.loop = false;
        $playerRepeatSpan.querySelector("svg").style.stroke = "";
        $playerRepeatSpan.querySelector("svg").style.fill = "";
    }
    if (isShuffle) { // 셔플이 이미 진행중이라면 셔플기능을 해제하고, 아이콘 또한 원래대로 바꾼다.
        isShuffle = false;
        $playerShuffleSpan.querySelector("svg").style.stroke = "";
        $playerShuffleSpan.querySelector("svg").style.fill = "";
    }else{ // 셔플이 진행중이 아니라면셔플기능을 설정해주고, 아이콘 또한 색을 변경한다.
        isShuffle = true;
        $playerShuffleSpan.querySelector("svg").style.stroke = "skyblue";
        $playerShuffleSpan.querySelector("svg").style.fill = "skyblue";


    }
}

// 모든 플레이리스트의 정보를 담고있는 json 데이터를 가져와 처리하는 함수
function loadPlayList() {
    fetch("./data/playlists.json")
        .then(value => value.json())
        .then(json => {
            json.playlists.map((v)=>{ // 각 플레이리스트 정보를 이용하여 플레이리스트 표시에 필요한 노드를 생성한다.
                let $li = document.createElement("li");
                let $a= document.createElement("a");
                let $i = document.createElement("i");
                let order =v.order;
                let playlistTitle = v.playlistTitle;
                let musicIndices = v.musicIndices;
                $i.setAttribute("data-feather", "music");
                $a.append($i,playlistTitle);
                // $a.innerText = playlistTitle;
                $a.style.cursor = "pointer";
                // 플레이리스트 노드에는 해당 플레이리스트가 어떤 음악 인덱스를 가지고있는지 설정으로 가지고있는다.
                $a.setAttribute("data-music-indices", musicIndices);
                $li.append($a);

                $li.addEventListener('click',clickPlayList); // 클릭 이벤트 설정
                document.querySelector("#playlist").append($li);
                feather.replace();

            })
        });
}

// 모든 음악 목록을 보여주는 함수 ( All music 클릭 이벤트 리스너 함수 )
function showAllMusic() {
    let artworks = document.querySelector(".artworks");
    for (let artwork of artworks.children) {
        artwork.style.display = ""; // 모든 음악 article 이 보이게 설정
    }
    document.querySelector(".screen-title").innerText = "All Music";
    currentPlayListIndices=[]; //음악리스트배열을 초기화하고
    for (let index = 0; index < totalMusicCount; index++) {
        // 모든음악을 담는 하나의 플레이리스트라고 취급하고 현재 플레이리스트 인덱스 배열에 음악 전체 index를 담는다.
        currentPlayListIndices.push(index);
    }
    console.log("currentPlayListIndices : " + currentPlayListIndices);
    console.log("musicIndex : " + musicIndex);
    console.log("totalMusicCount : " + totalMusicCount);
    currentPlayListMusicCount = totalMusicCount; // 플레이리스트 음악개수를 전체음악개수로 초기화해준다.

}


// 플레이리스트 클릭 이벤트 리스너 함수 : 해당 플레이리스트 클릭시 플레이리스트에 해당하는 음악만 보여준다.
function clickPlayList(event) {
    // data 속성에서 해당 플레이리스트 음악 인덱스들을 가져온다.
    let musicIndices = event.target.getAttribute("data-music-indices").split(",");

    //모든 음악(article 노드)가 담긴 노드를 가져온다.
    let $artworks = document.querySelector(".artworks");

    currentPlayListIndices=[...musicIndices]; //클릭된 플레이리스트의 음악들 인덱스를 담는다
    currentPlayListMusicCount = musicIndices.length; // 클릭된 플레이리스트의 음악들 갯수를 담는다.
    console.log("musicIndex : " + musicIndex);
    console.log("currentPlayListIndices : "+currentPlayListIndices);
    console.log("currentPlayListMusicCount : "+currentPlayListMusicCount);
    // 모든 음악(article)중 플레이리스트에 포함되지않는 음악이면 display : none 으로 처리하여 안보이게 한다.
    for (let artwork of $artworks.children) {
        // console.log(artwork);
        let musicIndex = artwork.getAttribute("data-article-index");
        if (!musicIndices.includes(musicIndex)) {
            artwork.style.display = "none";
        } else {
            artwork.style.display = "";
        }
    }
    let element = document.querySelector(".screen-title");
    console.log("event.target.innerText : "+event.target.innerText);
    element.innerText =  `${event.target.innerText}'s playlist`;
}

//아이패드 볼륨버튼으로 볼륨조절하는 이벤트 리스너 함수
function volumeManage(event) {
    // 오디오에서 현재 볼륨값을 가져오던가, 볼륨바에서 현재 width 를 가져오든가해서 현재 볼륨값을 가져와서 구현하든 선택이다.
    let currentVolume = $volumeProgressBarDoneDiv.style.width.slice(0,-1);
    console.log("currentVolume : " +currentVolume);
    console.log("event.target.className : "+event.target.className)
    //업버튼 클릭시 볼륨 10 증가 , 10 증가시 100보다 클때는 100으로 설정
    if (event.target.className === "upButton") {
        if (Number(currentVolume) + 10 < 100) {
            $audio.volume = (Number(currentVolume) + 10) * 0.01;
            $volumeProgressBarDoneDiv.style.width = `${Number(currentVolume) + 10}%`;
        } else {
            $audio.volume = 1;
            $volumeProgressBarDoneDiv.style.width = `${100}%`;
        }
    } else { // 다운 버튼 클릭시 볼륨 10 감소, 10 감소시 0보다 작다면 0으로 설정
        console.log(Number(currentVolume) - 10);
        if (Number(currentVolume) - 10 > 0) {
            $audio.volume = (Number(currentVolume) - 10) * 0.01;
            $volumeProgressBarDoneDiv.style.width = `${Number(currentVolume) - 10}%`;
        } else {
            $audio.volume = 0;
            $volumeProgressBarDoneDiv.style.width = `${0}%`;
        }


    }
    // console.log($volumeProgressBarDoneDiv.style.width);


    // $volumeProgressBarDoneDiv.style.width= `${(clickX/volumeBarWidth)*100}%`;
}

export function getReadyMusicPlayer() {
    $audio = document.querySelector("#main-audio");
    //music player 관련 노드
    $skipBackBtn = document.querySelector(".skip-back-btn");
    $playBtn = document.querySelector(".play-btn");
    $skipForwardBtn = document.querySelector(".skip-forward-btn");

    //플레이어독 왼쪽 음악정보 부분 노드
    $songCoverImage = document.querySelector(".player__pic");
    $songArtist = document.querySelector(".now-playing--artist");
    $songTitle = document.querySelector(".now-playing--title");

    //음악시간 관련 노드
    $currentMusicTimeSpan = document.querySelector("#currentMusicTime");
    $currentMusicTotalTimeSpan = document.querySelector("#totalMusicTime");
    $progressBarDiv = document.querySelector('#music-progress-bar');
    $playerProgressBarDoneDiv = document.querySelector("#music-progress-bar-done");

    //볼륨관련 노드
    $volumeBtnSpan = document.querySelector('#volumeBtn');
    $volumeProgressBarDiv = document.querySelector('#volume-progress-bar');
    $volumeProgressBarDoneDiv = document.querySelector('#volume-progress-bar-done');

    //한곡반복 , 셔플
    $playerRepeatSpan = document.querySelector(".player-repeat");
    $playerShuffleSpan = document.querySelector(".player-shuffle");
    //음악검색
    $musicSearchInput = document.querySelector(".music-search");

    //플레이리스트 , 전체음악
    $allmusicA = document.querySelector(".all-music");
    $playListsNav = document.querySelector("#play-lists");

    //메인화면 앨범커버
    $artworks = document.querySelector(".artworks");

    //볼륨 업다운
    $volumeUp = document.querySelector(".upButton");
    $volumeDown = document.querySelector(".downButton");



    musicIndex =0;
    currentPlayListIndices=[];
    articleIndex =0;
    isShuffle = false;


    loadMusics().then(musics => {
        let articles = musics.map(createArticle);
        let element = document.querySelector(".artworks");
        currentPlayListMusicCount = currentPlayListIndices.length;
        element.append(...articles);
        initAudio();
        loadPlayList();
        console.log(musicJsonArray);
    }).then( () =>{
        //재생버튼
        $playBtn.addEventListener('click',playMusic);
        //다음곡버튼
        $skipForwardBtn.addEventListener('click', playNextMusic);
        //이전곡버튼
        $skipBackBtn.addEventListener('click', playBeforeMusic);
        //음악시간, 프로그래스바 업데이트
        $audio.addEventListener('timeupdate',updateMusicProgressbar);
        $audio.addEventListener('loadeddata',updateDuration);
        //음악 프로그레스바 클릭
        $progressBarDiv.addEventListener('click', musicProgressBarClick);
        //볼륨버튼
        $volumeBtnSpan.addEventListener('click', mute);
        //볼륨프로그레스바 클릭
        $volumeProgressBarDiv.addEventListener('click', volumeBarClick);
        //노래재생끝났을때
        $audio.addEventListener('ended', playNextMusic);
        //한곡반복, 셔플
        $playerRepeatSpan.addEventListener('click', repeat);
        $playerShuffleSpan.addEventListener('click', shuffle);

        //음악 전체보기
        $allmusicA.addEventListener('click',showAllMusic)
        //아이패드 볼륨버튼설정
        $volumeUp.addEventListener('click',volumeManage);
        $volumeDown.addEventListener('click',volumeManage);




        feather.replace();

        document.querySelectorAll(".player__dock").forEach((el) => {
            el.addEventListener("click", (e) => {
                document.querySelector(".player").classList.toggle("player--docked");
            });
        });

    })



}





