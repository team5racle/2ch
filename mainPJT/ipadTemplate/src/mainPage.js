import { introduce } from "./introduce.js";
import { signup } from "./signup.js";
import { video, killDrag } from "./video.js";
import { kanbanBoardReady } from "./kanban.js";
import { runCanvas } from "./canvas.js";
import { geolocation } from "./geolocation.js";
import { getReadyMusicPlayer } from "./audio.js";

const homeBackgroundImage =
    "url('https://images.unsplash.com/photo-1483728642387-6c3bdd6c93e5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2076&q=80')";

function powerMe() {
    const $inAppBar = document.getElementById("inAppBar");
    $inAppBar.style.bottom = "-8%";
    $inAppBar.style.pointerEvents = "none";

    document.getElementsByClassName("wallpaper")[0].style.backgroundImage =
        homeBackgroundImage;

    let $ipadScreen = document.getElementsByClassName("ipadScreen")[0];
    let $lockScreen = document.getElementsByClassName("lockScreen")[0];
    let $dockWrapper = document.getElementsByClassName("dockWrapper")[0];

    if (powerVar == 1) {
        // 디스플레이 꺼진 상태에서 display on
        $ipadScreen.style.opacity = 1;
        $ipadScreen.style.pointerEvents = "all";
        powerVar = 0;
        $lockScreen.style.transition =
            "top 800ms ease-in 0s, backdrop-filter 200ms ease-in 0s";
        $dockWrapper.style.transition = "bottom 400ms ease-in-out 0s";
        document.querySelector(".widget-todo-section").style.transition =
            "top 400ms ease-in-out 0s";
    } else {
        // display off
        $ipadScreen.style.opacity = 0;
        $ipadScreen.style.pointerEvents = "none";
        powerVar = 1;

        setTimeout(function () {
            $lockScreen.style.transition = "none";
            $lockScreen.style.backdropFilter = "blur(0)";
            $lockScreen.style.top = "0";
            $dockWrapper.style.transition = "none";
            $dockWrapper.style.bottom = "-20%";
            document.querySelector(".widget-todo-section").style.transition =
                "none";
            document.querySelector(".widget-todo-section").style.top = "-60%";
        }, 300);
    }
    resetIcons();
}

function lockClick() {
    document.getElementsByClassName("lockScreen")[0].style.backdropFilter =
        "blur(2vh) brightness(1.2)";
    document.getElementsByClassName("lockScreen")[0].style.top = "-110%";
    document.getElementsByClassName("dockWrapper")[0].style.bottom = "3%";
    document.querySelector(".widget-todo-section").style.top = "5%";
}

//잠금화면 시간 (Do not Touch)
function checkTime() {
    let day = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];
    let month = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    let d = new Date();
    let minTime = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
    document.getElementsByClassName("lockTime")[0].innerHTML =
        d.getHours() +
        ":" +
        minTime +
        "<br/>" +
        day[d.getDay()] +
        ", " +
        month[d.getMonth()] +
        " " +
        d.getDate();
    setTimeout(function () {
        checkTime();
    }, 500);
}

//inAppBar를 클릭 시 홈 화면으로 돌아가는 기능
function goHome() {
    document.getElementById("inAppBar").style.bottom = "-8%";
    document.getElementById("inAppBar").style.pointerEvents = "none";
    document.getElementsByClassName("dockWrapper")[0].style.bottom = "3%";
    document.querySelector(".widget-todo-section").style.top = "5%";
    // document.getElementsByClassName("dockWrapper")[0].style.top = ""; 범인을 동적으로 설정하도록 변경
    resetIcons();
    // document.getElementsByClassName("wallpaper")[0].style.backgroundImage = homeBackgroundImage;
}

function resetIcons() {
    document.querySelectorAll(".iconDivide").forEach(($iconDivide) => {
        $iconDivide.style.display = "block";
    });

    document.getElementById("inAppBar").style.zIndex = -1;
    const $$iconDiv = document.querySelectorAll(".iconDiv");
    $$iconDiv.forEach(function (x) {
        x.style.width = "calc(var(--sizeVar) / 20)";
        x.style.height = "calc(var(--sizeVar) / 20)";
        x.style.margin = "calc(var(--sizeVar) / 60) calc(var(--sizeVar) / 120)";
        x.style.pointerEvents = "all";
        x.classList.add("iconDivHover"); //호버 추가

        const $iconApp = x.querySelector(".appIcon");
        $iconApp.style.opacity = 1;

        const $section = x.querySelector(".sectionContainer");
        $section.style.pointerEvents = "none";
        $section.style.zIndex = -20;
        $section.style.opacity = 0;

        if ($section.style.width !== 0) {
            setTimeout(function () {
                $section.style.width = 0;
                $section.style.height = 0;
            }, 350);
        }

        if (powerVar == 1) {
            console.log(powerVar);
            x.style.transition =
                "all 0s ease-in-out 350ms, transform 100ms ease-in-out 0s";
            $iconApp.style.transition =
                "all 0s ease-in-out 350ms, transform 100ms ease-in-out 0s";
            $section.style.removeProperty("transition");
            $section.style.transition = "opacity 300ms linear 0s";
        } else {
            x.style.transition =
                "all 500ms ease-in-out 0s, transform 100ms ease-in-out 0s";
            $iconApp.style.transition =
                "all 500ms ease-in-out 0s, transform 100ms ease-in-out 0s";
            $section.style.removeProperty("transition");
        }
    });
}

//아이콘 클릭 시 동작
function iconClick(e) {
    console.log($(this).attr("id"));
    window.barColor = sectionBarColor[$(this).attr("id")];

    const $$iconDiv = document.querySelectorAll(".iconDiv");
    $$iconDiv.forEach(function (x) {
        x.style.width = 0;
        x.style.height = 0;
        x.style.margin = 0;
        x.style.pointerEvents = "none";
        x.querySelector(".appIcon").style.opacity = 0;
        x.classList.remove("iconDivHover"); //호버 제거
    });

    const $iconDiv = e.target.parentNode;
    $iconDiv.style.width = "calc(var(--sizeVar) * 1)";
    $iconDiv.style.height = "calc(var(--sizeVar) * .74)";
    // $iconDiv.style.marginLeft = "calc(var(--sizeVar) / 23.5)";
    document.getElementsByClassName("dockWrapper")[0].style.bottom = 0;
    // document.getElementsByClassName("dockWrapper")[0].style.top = 0; //범인을 동적으로 설정하도록 변경

    const $section = $iconDiv.querySelector(".sectionContainer");
    $section.style.width = "calc(var(--sizeVar) * 1)";
    $section.style.height = "calc(var(--sizeVar) * .74)";
    $section.style.pointerEvents = "all"; //section pointer 기능 활성화
    $section.style.zIndex = 20;
    $section.style.opacity = 1;

    const $inAppBar = document.getElementById("inAppBar");
    $inAppBar.style.setProperty("--colorMe", window.barColor);
    $inAppBar.style.bottom = "0";
    $inAppBar.style.pointerEvents = "all";
    $inAppBar.style.zIndex = 100;

    document.querySelector(".widget-todo-section").style.top = "-60%"; //위젯 todo
    document.querySelectorAll(".iconDivide").forEach(($iconDivide) => {
        $iconDivide.style.display = "none";
    });

    // setTimeout(function () {
    // document.getElementsByClassName("wallpaper")[0].style.backgroundImage =
    // "none";
    // }, 500);
}

let powerVar = 0; // 0: 전원 켜짐 상태 / 1: 전원 꺼짐 상태

// 처음 자동으로 화면 켜짐
setTimeout(function () {
    document.getElementsByClassName("ipadScreen")[0].style.opacity = 1;
}, 1250);

window.onload = function () {
    // let powerVar = ; // 0: 전원 켜짐 상태 / 1: 전원 꺼짐 상태

    document.querySelector(".mainBody").onload = checkTime();
    document.querySelector(".powerButton").addEventListener("click", powerMe);
    document.querySelectorAll(".appIcon").forEach(function ($appIcon) {
        $appIcon.addEventListener("click", iconClick);
        // $appIcon.addEventListener('click', window.barColor="white");
    });
    document.querySelector(".lockScreen").addEventListener("click", lockClick);
    document.querySelector("#inAppBar").addEventListener("click", goHome);
};

// 페이지 별 bar color
const sectionBarColor = {
    appIconGeolocation: "black",
    appIconVideo: "white",
    appIconAudio: "white",
    appIconIntroduce: "white",
    appIconBoard: "white",
    appIconSignup: "white",
    appIconCanvas: "black",
    appIconKanban: "white",
};

// 파일 경로 추가 - 해당 코드만 조작
const sectionIdName = {
    "app-geolocation": "./apps/geolocation.html",
    "app-video": "./apps/video.html",
    "app-audio": "./apps/audio.html",
    "app-introduce": "./apps/introduce.html",
    "app-board": "./apps/board.html",
    "app-signup": "./apps/signup.html",
    "app-canvas": "./apps/canvas.html",
    "app-kanban": "./apps/todoKanban.html",
};

$(document).ready(function () {
    function removeComponent() {
        killDrag();
        $(".sectionContainer").each(function () {
            $(this).empty();
        });
    }

    $("#inAppBar").on("click", removeComponent);
    $(".appIcon").each(function () {
        $(this).on("click", function () {
            const $appIcon = $(this).next();
            const id = $appIcon.attr("id"); // id 변수로 뻄
            const addr = sectionIdName[id];
            $appIcon.load(addr);

            setTimeout(function () {
                if (id === "app-geolocation") {
                    setTimeout(
                        () =>
                            kakao.maps.load(function () {
                                geolocation();
                            }),
                        1000
                    ); // 1초 뒤에 호출
                } else if (id === "app-video") {
                    setTimeout(() => video(), 1000); //1초 뒤에 호출
                } else if (id === "app-introduce") {
                    setTimeout(() => introduce(), 1000); // 1초 뒤에 호출
                } else if (id === "app-kanban") {
                    setTimeout(() => kanbanBoardReady(), 300); // 0.3초 뒤에 호출
                } else if (id === "app-canvas") {
                    setTimeout(() => runCanvas(), 1000); // 1초 뒤에 호출
                } else if (id === "app-signup") {
                    setTimeout(() => signup(), 1000); // 1초 뒤에 호출
                } else if (id === "app-audio") {
                    setTimeout(() => getReadyMusicPlayer(), 300); // 0.3초 뒤에 호출
                }
            }, 10);
        });
    });
});
